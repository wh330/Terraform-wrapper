# Terraform Wrapper

A wrapper to run Terraform commands on multiple directories.

In the top-level directory, [.config/](https://gitlab.developers.cam.ac.uk/wh330/Terraform-wrapper/-/tree/master/tests/config) should contain the common terraform config files (backend, provider, variables, etc) of the terraform sub-directories. The env variables defined in the config files will be populated from the environment. See [https://gitlab.developers.cam.ac.uk/wh330/Terraform-wrapper/-/tree/master/tests](tests) as an example of a project structure. 

A live example of this image in action can be found on https://gitlab.developers.cam.ac.uk/uis/infra/sas-terraform/vm-provisioning.

## How to use
```bash
$ # Example of a project
$ tree example-project/
example-project/
├── config
│   └── backend.tpl
└── terraform-projects
    ├── project1
    │   └── main.tf
    └── project2
        └── main.tf

4 directories, 3 files
$ docker run -it --rm \
    -v $PWD/example-project:/workdir registry.gitlab.developers.cam.ac.uk/wh330/terraform-wrapper/releases/0.12 \
    terraform-all <terraform command>
```

## Release
This image supports terraform 0.12.

## Resources
- https://gitlab.com/gitlab-org/terraform-images/