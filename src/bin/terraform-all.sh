#!/usr/bin/env bash
#
# Wrapper script to run terraform command on multiple folders.
# 
# The script populates and copies the templates "provider.tpl", "pg_backend.tpl",
# "variables.tpl" to the terraform directories, then it runs the terraform
# command passed as arguments and finally clean up those directories
# by deleting the initially copied files.
#
# The env variables in the templates should be defined before running the script.

# The return code of the script
SCRIPT_RC=0

# To parse terraform plans
JQ_PLAN='
  (
    [.resource_changes[]?.change.actions?] | flatten
  ) | {
    "create":(map(select(.=="create")) | length),
    "update":(map(select(.=="update")) | length),
    "delete":(map(select(.=="delete")) | length)
  }
'

plan_cache="plan.cache"
plan_json_dir="$PWD/plan-json"

# create plan_json_dir
mkdir -p $plan_json_dir
rm -f $plan_json_dir/*

# Get Terraform directories. Any directory containing .tf files
TERRAFORM_DIRS="$(find ./*/ -not -path '*/\.*' -type f -name '*.tf' | sed -r 's|/[^/]+$||' | sort | uniq)"

# Get template files
TPL_FILES="$(find ./config -maxdepth 1 -type f -name '*.tpl' -exec basename {} \;)"

# Description: show help message
usage() {
cat << EOF
Usage: $0 [options] CMD1 CMD2 ...

A simple wrapper script that runs terraform commands on multiple folders.

OPTIONS:
-h                                  Show this help message
-d dir                              Optional. The dir in which to run the terraform command.
                                    This will be passed as [dir] option to Terraform.
                                    If not specified, run the Terraform commands on all
                                    the sub-directories.

E.g.
Run terraform plan in ./examples/test: $0 -d ./examples/test plan
Run terraform init on all the folders containing .tf files: $0 init
EOF
}

# Description: update and copy template files to a terraform module directory
# Args: dir
create_config_files() {
    # Set the env var PG_SCHEMA of the pg backend.
    # Conclude the PG_SCHEMA from $1. Ex: ./path/to/foo --> path_to_foo
    export PG_SCHEMA=$(echo ${1%/} | sed 's/\.\///g; s/\//_/g; s/\-/_/g')

    # Copy template files to terraform directory.
    for tpl_file in $TPL_FILES
    do
        local tf_file="$(echo $tpl_file | cut -f 1 -d '.').tf"

        # Just continue if the file already exists
        if ls $1/*$tf_file 1> /dev/null 2>&1; then
          continue
        fi

        cp "config/$tpl_file" "$1/auto.$tf_file"

        local tmpfile=$(mktemp)
        cat "$1/auto.$tf_file" | envsubst > "$tmpfile" && cp -pf "$tmpfile" "$1/auto.$tf_file"
        rm -f "$tmpfile"
    done
}

# Description: run terraform command in a directory
# Args: SELECTED_DIR
#       TF_CMDS
run_terraform() {
    # Copy common config files to terraform directories
    create_config_files $1

    case "${@:2}" in
      "apply")
        # It seems that the dir option wouldn't be considered when a plan is also specified
        (cd $1 &&
        echo -e "\n*** Run \"terraform ${@:2} ${plan_cache}\" in $1:\n"
        terraform ${@:2} "${plan_cache}"
        )
      ;;
      "plan")
        echo -e "\n*** Run \"terraform ${@:2} -out=$1/${plan_cache}\" in $1:\n"
        TF_DATA_DIR="$1/.terraform" terraform ${@:2} -out="$1/${plan_cache}" $1
      ;;
      "plan-json")
        (cd $1 &&
        local tmpfile=$(mktemp -p $plan_json_dir)
        echo -e "\n*** Run \"terraform show -json $1/${plan_cache}\" in $1:\n"
        echo "{" >> "$tmpfile"
        echo "\"$1\":" >> "$tmpfile"
        terraform show -json "${plan_cache}" | jq -r "${JQ_PLAN}" >> "$tmpfile"
        echo "}" >> "$tmpfile"
        )
      ;;
      *)
        echo -e "\n*** Run \"terraform ${@:2}\" in $1:\n"
        TF_DATA_DIR="$1/.terraform" terraform ${@:2} $1
      ;;
    esac

    return $?
}

# Parse the script args
while getopts "hd:c:" opt
do
  case $opt in
    h)
      usage
      exit
      ;;
    d)
      SELECTED_DIR=$OPTARG
     ;;
    ?)
      usage
      exit
      ;;
  esac
done

# Check if SELECTED_DIR is specified
if [ ! -z "$SELECTED_DIR" ]
then
  run_terraform $SELECTED_DIR "${@:$OPTIND}"
  exit $?
fi

for dir in $TERRAFORM_DIRS
do
  # SCRIPT_RC only gets updated if the terraform command fails
  run_terraform $dir "${@:$OPTIND}" || SCRIPT_RC=$?
done

exit $SCRIPT_RC
