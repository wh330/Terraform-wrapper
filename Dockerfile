ARG BASE=hashicorp/terraform:latest
FROM $BASE

RUN apk update && apk add bash gettext jq
  
COPY src/bin/terraform-all.sh /usr/bin/terraform-all
RUN chmod +x /usr/bin/terraform-all

# Set workdir in case the image would be used from CLI
WORKDIR /workdir

# Override ENTRYPOINT since hashicorp/terraform uses `terraform`
ENTRYPOINT []